# RODOS Docker container
This repo holds the build files and helper scripts for the RODOS development docker container.

## Usage
Copy the `rodos-env` script into your RODOS project.
Now the script can be used to build the project. Make sure you are in the root directory of your project. Then call the script with any cmd you would normaly use to build the project.
```
$ ./rodos-env ./scripts/do-the-thing.sh
```

## Building / Updating
Edit the Dockerfile to add/remove dependencies. Change the container-run.sh to change the entry point. Then build with:
```
$ docker build -t gitlab2.informatik.uni-wuerzburg.de:4567/info8-containers/rodos-docker:rodos .
```
Upload with (See the [Gitlab container registy docs](https://docs.gitlab.com/ee/user/packages/container_registry/) on how to authenticate):
```
$ docker push gitlab2.informatik.uni-wuerzburg.de:4567/info8-containers/rodos-docker:rodos
```

To add new dependencies for your own project, fork the `custom-template` branch and edit the Dockerfile.
