# syntax=docker/dockerfile:1
# Build fails in weird ways when using 22.04
FROM ubuntu:23.04

ENV DEBIAN_FRONTEND noninteractive
ENV TZ=UTC

RUN apt-get update
RUN yes | unminimize
RUN apt-get install -y apt-utils
RUN apt-get install -y build-essential gcc-multilib g++-multilib clang cmake man-db man \
                       libgmp3-dev libmpfr-dev libx11-6 libx11-dev \
                       texinfo flex bison libmpc-dev libncurses6 libncurses-dev libncursesw6 zlib1g \
                       binutils-arm-none-eabi gcc-arm-none-eabi libnewlib-arm-none-eabi
RUN apt-get -y autoremove
RUN apt-get clean
RUN rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

COPY container-run /usr/local/bin/container-run
RUN chmod +x /usr/local/bin/container-run

ENTRYPOINT [ "/usr/local/bin/container-run" ]
